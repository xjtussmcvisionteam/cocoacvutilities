//
//  NSValue+CVFormats.m
//  OpenFace
//
//  Created by JiangZhping on 16/7/30.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSValue+CVFormats.h"

#if TARGET_OS_IPHONE
#define valueWithRect valueWithCGRect
#define valueWithPoint valueWithCGPoint
#define valueWithSize valueWithCGSize
#define rectValue CGRectValue
#define sizeValue CGSizeValue
#define pointValue CGPointValue
#endif

@implementation NSValue (CVFormats)

+ (NSValue *) valueWithCvRect2d:(cv::Rect2d)cvRect {
    return [NSValue valueWithRect:CGRectMakeWithCvRect(cvRect)];
}

+ (NSValue *) valueWithCvPoint2d:(cv::Point2d)cvPoint {
    return [NSValue valueWithPoint:CGPointMakeWithCvPoint(cvPoint)];
}

+ (NSValue *) valueWithCvPoint3d:(cv::Point3d)cvPoint {
    return [NSValue valueWithSCNVector3:SCNVector3FromGLKVector3(GLKVector3MakeWithCvPoint3(cvPoint))];
}

+ (NSValue *) valueWithCvSize2d:(cv::Size2d)cvSize {
    return [NSValue valueWithSize: CGSizeMakeWithCvSize(cvSize)];
}

- (cv::Rect2i) cvRect2iValue {
    CGRect rect = self.rectValue;
    return CvRectMakeWithCGRect<int>(rect);
}

- (cv::Rect2f) cvRect2fValue {
    CGRect rect = self.rectValue;
    return CvRectMakeWithCGRect<float>(rect);
}

- (cv::Rect2d) cvRect2dValue {
    CGRect rect = self.rectValue;
    return CvRectMakeWithCGRect<double>(rect);
}

- (cv::Point2i) cvPoint2iValue {
    CGPoint point = self.pointValue;
    return CvPointMakeWithCGPoint<int>(point);
}

- (cv::Point2f) cvPoint2fValue {
    CGPoint point = self.pointValue;
    return CvPointMakeWithCGPoint<float>(point);
}

- (cv::Point2d) cvPoint2dValue {
    CGPoint point = self.pointValue;
    return CvPointMakeWithCGPoint<double>(point);
}

- (cv::Point3i) cvPoint3iValue {
    SCNVector3 vector = self.SCNVector3Value;
    return CvPoint3MakeWithGLKVector3<int>(SCNVector3ToGLKVector3(vector));
}

- (cv::Point3f) cvPoint3fValue {
    SCNVector3 vector = self.SCNVector3Value;
    return CvPoint3MakeWithGLKVector3<float>(SCNVector3ToGLKVector3(vector));
}

- (cv::Point3d) cvPoint3dValue {
    SCNVector3 vector = self.SCNVector3Value;
    return CvPoint3MakeWithGLKVector3<double>(SCNVector3ToGLKVector3(vector));
}

- (cv::Size2i) cvSize2iValue {
    CGSize size = self.sizeValue;
    return CvSizeMakeWithCGSize<int>(size);
}

- (cv::Size2f) cvSize2fValue {
    CGSize size = self.sizeValue;
    return CvSizeMakeWithCGSize<float>(size);
}

- (cv::Size2d) cvSize2dValue {
    CGSize size = self.sizeValue;
    return CvSizeMakeWithCGSize<double>(size);
}

#if TARGET_OS_IPHONE
#undef valueWithRect
#undef valueWithPoint
#undef valueWithSize
#undef rectValue
#undef sizeValue
#undef pointValue
#endif

@end
