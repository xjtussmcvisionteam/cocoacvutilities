//
//  CameraCalibratorView.m
//  CocoaCVUtilities
//
//  Created by JiangZhping on 2016/10/18.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CameraCalibratorView.h"

@implementation CameraCalibratorView

- (instancetype)initWithCoder:(NSCoder *)coder {
    self =[super initWithCoder:coder];
    if (self) {
        [self initializationJob];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frameRect {
    self = [super initWithFrame:frameRect];
    if (self) {
        [self initializationJob];
    }
    return self;
}

- (void) initializationJob {
    [self registerForDraggedTypes:@[NSURLPboardType]];
}

- (NSDragOperation)draggingUpdated:(id<NSDraggingInfo>)sender {
    NSPasteboard * pboard = [sender draggingPasteboard];
    if ([[pboard types] containsObject:NSURLPboardType]) {
        NSArray<NSURL *> * urls = [pboard readObjectsForClasses:@[[NSURL class]] options:nil];
        NSMutableArray<NSString *> * imageFilePaths = [NSMutableArray new];
        [urls enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString * path = [obj path];
            if ([[[path lowercaseString] pathExtension] isEqualToString:@"jpg"] || [[[path lowercaseString] pathExtension] isEqualToString:@"jpeg"] || [[[path lowercaseString] pathExtension] isEqualToString:@"png"]) {
                [imageFilePaths addObject:path];
            }
        }];
        
        if ([imageFilePaths count] > 0) {
            return NSDragOperationEvery;
        }
    }
    return NSDragOperationNone;
}

-(BOOL)performDragOperation:(id<NSDraggingInfo>)sender {
    NSPasteboard * pboard = [sender draggingPasteboard];
    NSMutableArray<NSString *> * imageFilePaths = [NSMutableArray new];
    if ([[pboard types] containsObject:NSURLPboardType]) {
        NSArray<NSURL *> * urls = [pboard readObjectsForClasses:@[[NSURL class]] options:nil];
        [urls enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString * path = [obj path];
            if ([[[path lowercaseString] pathExtension] isEqualToString:@"jpg"] || [[[path lowercaseString] pathExtension] isEqualToString:@"jpeg"] || [[[path lowercaseString] pathExtension] isEqualToString:@"png"]) {
                [imageFilePaths addObject:path];
            }
        }];
    }
    
    if (!_imageFilePaths) {
        _imageFilePaths = [NSMutableArray new];
    } else {
        [_imageFilePaths performSelector:@selector(removeAllObjects)];
    }
    
    [_imageFilePaths performSelector:@selector(addObjectsFromArray:) withObject:imageFilePaths];
    
    return NO;
}

@end
