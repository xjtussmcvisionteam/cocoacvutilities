//
//  CameraCalibratorView.h
//  CocoaCVUtilities
//
//  Created by JiangZhping on 2016/10/18.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <CocoaXtensions/CocoaXtensions.h>

@interface CameraCalibratorView : NSUIView<NSDraggingDestination>

@property (nonatomic) NSArray<NSString *> * imageFilePaths;

@end
