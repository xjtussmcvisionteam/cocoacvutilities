//
//  main.m
//  CameraCalibrator
//
//  Created by JiangZhping on 2016/10/18.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
