//
//  CMAttitude+OpenCVMat.h
//  CocoaCVUtilities
//
//  Created by JiangZhping on 2016/9/30.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>
#import <opencv2/opencv.hpp>
#import <CoreOpenCVExtensions/CoreOpenCVExtensions.h>

@interface CMAttitude (OpenCVMat)

@property (nonatomic, readonly) cv::Matx33d cvRotationMatrix;

@end
