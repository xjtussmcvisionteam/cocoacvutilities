//
//  CvVectorGeometry.m
//  VADSDelegates
//
//  Created by JiangZhiping on 16/4/2.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CvVectorGeometry.h"

@implementation CvVectorGeometry

+ (cv::Vec3d) intersectionPointBetweenVectorAndPlane_VectorInitialPoint:(const cv::Vec3d &)vectorInitialPoint VectorDirection:(const cv::Vec3d &)vectorDirectionMat PlaneZeroPoint:(const cv::Vec3d &)planeZeroPointMat PlaneNormalVector:(const cv::Vec3d &)planeNormalVector {
    
    return [CvVectorGeometry intersectionPointBetweenVectorAndPlane_VectorInitialPoint:vectorInitialPoint VectorDirection:vectorDirectionMat PlaneZeroPoint:planeZeroPointMat PlaneNormalVector:planeNormalVector ForcePositiveDirection:YES];
}

+ (cv::Vec3d) intersectionPointBetweenVectorAndPlane_VectorInitialPoint:(const cv::Vec3d &)vectorInitialPoint VectorDirection:(const cv::Vec3d &)vectorDirectionMat PlaneZeroPoint:(const cv::Vec3d &)planeZeroPointMat PlaneNormalVector:(const cv::Vec3d &)planeNormalVector ForcePositiveDirection:(BOOL)forcePositive {
    
    cv::Vec3d normalizedVectorDirectionMat = vectorDirectionMat * ( 1.0/ cv::norm(vectorDirectionMat));
    cv::Vec3d normalizedPlaneNormalVector = planeNormalVector * (1.0/ cv::norm(planeNormalVector));
    double vectorLength = (planeZeroPointMat - vectorInitialPoint).dot(normalizedPlaneNormalVector) /
    normalizedVectorDirectionMat.dot(normalizedPlaneNormalVector);
    
    cv::Vec3d returnValue;
    if (forcePositive) {
        if (vectorLength > 0) {
            returnValue = vectorInitialPoint + normalizedVectorDirectionMat * vectorLength;
        } else {
            returnValue = cv::Vec3d();
        }
    } else {
        returnValue = vectorInitialPoint + normalizedVectorDirectionMat * vectorLength;
    }
    
    return returnValue;
}

+ (cv::Vec3d) irisCenterPixelCoordinate:(const cv::Vec2d &)irisCenterPoint InFaceFrameWithCameraIntrinsicParameter:(const cv::Matx33d &)intrinsicParameter  FacePose:(const cv::Matx33d &)poseFaceToFrontCamera AndFaceTranslation:(const cv::Vec3d &)translationFaceToFrontCamera {
    
    cv::Vec3d irisCenterPixelHomo = cve::vec::changeDimension<3>(irisCenterPoint, 1.0);
    cv::Vec3d irisCenterInUVHomo = intrinsicParameter.inv() * irisCenterPixelHomo;
    cv::Vec3d facePlanNormInFrontCameraFrame(0.0, 0.0, 1.0);
    facePlanNormInFrontCameraFrame = poseFaceToFrontCamera * facePlanNormInFrontCameraFrame ;
    cv::Vec3d irisCenterInFrontCameraFrame = [CvVectorGeometry intersectionPointBetweenVectorAndPlane_VectorInitialPoint:cv::Vec3d() VectorDirection:irisCenterInUVHomo PlaneZeroPoint:translationFaceToFrontCamera PlaneNormalVector:facePlanNormInFrontCameraFrame];
    if (irisCenterInFrontCameraFrame == cv::Vec3d()) {
        return cv::Vec3d();
    }
    cv::Vec3d irisCenterInFace = cve::matx::transformVectorWithTransformMat(cve::matx::joinRotationMatAndTranslationVec(poseFaceToFrontCamera, translationFaceToFrontCamera), irisCenterInFrontCameraFrame);
    
    return irisCenterInFace;
}

+ (double) distanceBetweenAPoint:(const cv::Vec3d &)pointA AndPlaneB_WithNormalVector:(const cv::Vec3d &)normalVector AndPlaneZeroPoint:(const cv::Vec3d &)zeroPoint{
    cv::Vec3d pointAfromZero = pointA - zeroPoint;
    cv::Vec3d unitNormal = normalVector/cv::norm(normalVector);
    double verticalDistance = pointAfromZero.dot(unitNormal);
    
    return verticalDistance;
}

+ (cv::Vec3d) projectionOfPoint:(const cv::Vec3d & )pointA OntoAPlaneWithNormalVector:(const cv::Vec3d &)normalVector AndPlaneZeroPoint:(const cv::Vec3d &)zeroPoint {
    cv::Vec3d pointAfromZero = pointA - zeroPoint;
    cv::Vec3d unitNormal = normalVector/cv::norm(normalVector);
    double verticalDistance = pointAfromZero.dot(unitNormal);
    cv::Vec3d projectionPoint = pointA - unitNormal * verticalDistance;
    
    return projectionPoint;
}

+ (cv::Vec3d) projectionOfVector:(const cv::Vec3d &)vectorA OntoAnotherVector:(const cv::Vec3d &)vectorB {
    return vectorA.dot(vectorB) / cv::sqrt(cv::norm(vectorB)) * vectorB;
}

+ (cv::Vec3d) projectionOfVector:(const cv::Vec3d &)vectorA OntoAPlaneWithNormalVector:(const cv::Vec3d &)normalVector {
    return vectorA - [CvVectorGeometry projectionOfVector:vectorA OntoAnotherVector:normalVector];
}

+ (double) radianBetweenVector:(const cv::Vec3d &)vectorA AndVector:(const cv::Vec3d)vectorB {
    double arccosine = vectorA.dot(vectorB) / (cv::norm(vectorA) * cv::norm(vectorB));
    return acos(arccosine);
}

+ (cv::Vec2d) rotateScreenPixel:(const cv::Vec2d &)pixel WithRadian:(double)radian {
    cv::Vec3d homoPixelCoordiante = cve::vec::changeDimension<3>(pixel, 0.0);
    cv::Vec3d eulerAngle(0,radian,0);
    cv::Matx33d rotationMatrix = cve::eulerAngles2RotationMatrix(eulerAngle);
    cv::Vec3d rotatedHomoPixel = rotationMatrix * homoPixelCoordiante;
    
    return cve::vec::changeDimension<2>(rotatedHomoPixel);
}

+ (cv::Vec2d) rotateScreenPixel:(const cv::Vec2d &)pixel With3x3RotationMatrix:(const cv::Matx33d &)rotationMatrix {
    cv::Vec3d homoPixelCoordinate = cve::vec::changeDimension<3>(pixel, 1.0);
    cv::Vec3d rotatedHomoPixel = rotationMatrix * homoPixelCoordinate;
    
    return cve::vec::changeDimension<2>(rotatedHomoPixel);
}


@end
