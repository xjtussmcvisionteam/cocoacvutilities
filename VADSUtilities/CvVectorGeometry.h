//
//  CvVectorGeometry.h
//  VADSDelegates
//
//  Created by JiangZhiping on 16/4/2.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>
#import <CoreOpenCVExtensions/CoreOpenCVExtensions.h>

@interface CvVectorGeometry : NSObject

+ (cv::Vec3d) intersectionPointBetweenVectorAndPlane_VectorInitialPoint:(const cv::Vec3d &)vectorInitialPoint VectorDirection:(const cv::Vec3d &)vectorDirectionMat PlaneZeroPoint:(const cv::Vec3d &)planeZeroPointMat PlaneNormalVector:(const cv::Vec3d &)planeNormalVector;

+ (cv::Vec3d) intersectionPointBetweenVectorAndPlane_VectorInitialPoint:(const cv::Vec3d &)vectorInitialPoint VectorDirection:(const cv::Vec3d &)vectorDirectionMat PlaneZeroPoint:(const cv::Vec3d &)planeZeroPointMat PlaneNormalVector:(const cv::Vec3d &)planeNormalVector ForcePositiveDirection:(BOOL)forcePositive;

+ (cv::Vec3d) irisCenterPixelCoordinate:(const cv::Vec2d &)irisCenterPoint InFaceFrameWithCameraIntrinsicParameter:(const cv::Matx33d &)intrinsicParameter  FacePose:(const cv::Matx33d &)poseFaceToFrontCamera AndFaceTranslation:(const cv::Vec3d &)translationFaceToFrontCamera;

+ (double) distanceBetweenAPoint:(const cv::Vec3d &)pointA AndPlaneB_WithNormalVector:(const cv::Vec3d &)normalVector AndPlaneZeroPoint:(const cv::Vec3d &)zeroPoint;

+ (cv::Vec3d) projectionOfPoint:(const cv::Vec3d & )pointA OntoAPlaneWithNormalVector:(const cv::Vec3d &)normalVector AndPlaneZeroPoint:(const cv::Vec3d &)zeroPoint;

+ (cv::Vec3d) projectionOfVector:(const cv::Vec3d &)vectorA OntoAnotherVector:(const cv::Vec3d &)vectorB;

+ (cv::Vec3d) projectionOfVector:(const cv::Vec3d &)vectorA OntoAPlaneWithNormalVector:(const cv::Vec3d &)normalVector;

+ (double) radianBetweenVector:(const cv::Vec3d &)vectorA AndVector:(const cv::Vec3d)vectorB;

+ (cv::Vec2d) rotateScreenPixel:(const cv::Vec2d &)pixel WithRadian:(double)radian;
+ (cv::Vec2d) rotateScreenPixel:(const cv::Vec2d &)pixel With3x3RotationMatrix:(const cv::Matx33d &)rotationMatrix;

@end
