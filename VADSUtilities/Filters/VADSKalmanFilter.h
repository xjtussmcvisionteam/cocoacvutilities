//
//  VADSKalmanFilter.h
//  VADSDelegates
//
//  Created by JiangZhiping on 16/3/28.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>

@interface VADSKalmanFilter : NSObject

@property (nonatomic) double processNoiseCov;
@property (nonatomic) double measurementNoiseCov;
@property (nonatomic) cv::Mat & transitionMatrix;

- (instancetype) initWithNumberofParameters:(int) number;

- (instancetype) initWithNumberofParameters:(int)number AndCVType:(int)dtype;

- (instancetype) initWithNumberOf2DPoints:(int)numberOfPoints;

- (instancetype) initWithNumberOf3DPoints:(int)numberOfPoints;

- (instancetype) initWithNumberOf2DPoints:(int)numberOfPoints AndCVType:(int)dtype;

- (instancetype) initWithNumberOf3DPoints:(int)numberOfPoints AndCVType:(int)dtype;

- (cv::Mat) smooth:(const cv::Mat &) measurementMat;

- (std::vector<cv::Point2f>) smoothVectorOfPoint2f:(const std::vector<cv::Point2f> &) measurementPoints ;

- (std::vector<cv::Point3f>) smoothVectorOfPoint3f:(const std::vector<cv::Point3f> &) measurementPoints ;

@end
