//
//  VADSMedianMeanSmoother.h
//  VADSDelegates
//
//  Created by JiangZhiping on 16/3/30.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>
#import <CoreOpenCVExtensions/CoreOpenCVExtensions.h>

@interface VADSMedianMeanSmoother : NSObject

+ (cv::Point2f) smoothingPoint:(cv::Point2f)aPoint ForKey:(NSString *)aStringKey TrajectoryLength:(int)tLength AndMedianLength:(int)mLength;

+ (cv::Mat1d) smoothingVector:(cv::Mat1d)aVectorMat ForKey:(NSString *)aStringKey TrajectoryLength:(int)trajectoryLength AndMedianLength:(int)trajectoryMedianLength;

@end
