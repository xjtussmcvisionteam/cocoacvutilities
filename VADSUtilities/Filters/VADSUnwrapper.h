//
//  VADSUnwrapper.h
//  VADSDelegates
//
//  Created by JiangZhiping on 16/4/2.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>

@interface VADSUnwrapper : NSObject

+ (cv::Mat1d) unwrap:(cv::Mat1d &) wrappedVectorMat;

@end
