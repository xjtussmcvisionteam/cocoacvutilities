//
//  VADSKalmanFilter.m
//  VADSDelegates
//
//  Created by JiangZhiping on 16/3/28.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "VADSKalmanFilter.h"

@implementation VADSKalmanFilter {
    cv::KalmanFilter * filter;
    int count;
    int stateNumber;
}

- (instancetype) initWithNumberOf2DPoints:(int)numberOfPoints {
    return [self initWithNumberofParameters:numberOfPoints *2];
}

- (instancetype) initWithNumberOf3DPoints:(int)numberOfPoints {
    return [self initWithNumberofParameters:numberOfPoints *3];
}

- (instancetype) initWithNumberOf2DPoints:(int)numberOfPoints AndCVType:(int)dtype {
    return [self initWithNumberofParameters:numberOfPoints*2 AndCVType:dtype];
}

- (instancetype) initWithNumberOf3DPoints:(int)numberOfPoints AndCVType:(int)dtype {
    return [self initWithNumberofParameters:numberOfPoints*3 AndCVType:dtype];
}

- (instancetype) initWithNumberofParameters:(int)number {
    return [self initWithNumberofParameters:number AndCVType:CV_64F];
}

- (instancetype) initWithNumberofParameters:(int)number AndCVType:(int)dtype {
    
    self = [super init];
    if (self) {
        int depth = CV_MAT_DEPTH(dtype);
        filter = new cv::KalmanFilter(number *2, number, 0, depth);
        stateNumber = number;
        // first, constructing the transition matrix, consisting of 4 blocks.
        cv::Mat topLeft = cv::Mat::eye(number, number, depth);
        cv::Mat topRight, bottomRight;
        topLeft.copyTo(topRight);
        topLeft.copyTo(bottomRight);
        cv::Mat bottomLeft = cv::Mat::zeros(number, number, depth);
        
        cv::Mat top, bottom, transitionMatrix;
        cv::hconcat(topLeft, topRight, top);
        cv::hconcat(bottomLeft, bottomRight, bottom);
        cv::vconcat(top, bottom, transitionMatrix);
        transitionMatrix.copyTo(self.transitionMatrix);
        filter->transitionMatrix = self.transitionMatrix;
        cv::setIdentity(filter->measurementMatrix);
        
        cv::setIdentity(filter->errorCovPre,cv::Scalar::all(0.1f));
        self.processNoiseCov = 1e-4f;
        self.measurementNoiseCov = 0.01f;
    }
    return self;
}

- (cv::Mat) smooth:(const cv::Mat &) measurementMat {
    
    cv::Mat estimated;
    
    if (count++ == 0) {
        measurementMat.copyTo(filter->statePre);
//        measurementMat.copyTo(filter->statePost);
    }
    filter->predict();
    filter->correct(measurementMat).rowRange(0, stateNumber).copyTo(estimated);
    
    return estimated;
}

- (std::vector<cv::Point2f>) smoothVectorOfPoint2f:(const std::vector<cv::Point2f> &) measurementPoints {
    
    int insz[1] = {(int)measurementPoints.size()*2};
    int outsz[1] = {(int)measurementPoints.size()};
    cv::Mat pointsMat = cv::Mat(measurementPoints).reshape(1, 1, insz);
    cv::Mat smoothedPointsMat = [self smooth:pointsMat];
    std::vector<cv::Point2f> returnVector;
    smoothedPointsMat.reshape(2, 1, outsz).copyTo(returnVector);
    return returnVector;
}

- (std::vector<cv::Point3f>) smoothVectorOfPoint3f:(const std::vector<cv::Point3f> &) measurementPoints {
    
    int insz[1] = {(int)measurementPoints.size()*3};
    int outsz[1] = {(int)measurementPoints.size()};
    cv::Mat pointsMat = cv::Mat(measurementPoints).reshape(1, 1, insz);
    cv::Mat smoothedPointsMat = [self smooth:pointsMat];
    std::vector<cv::Point3f> returnVector;
    smoothedPointsMat.reshape(3, 1, outsz).copyTo(returnVector);
    return returnVector;
}

- (void) setProcessNoiseCov:(double)processNoiseCov {
    _processNoiseCov  = processNoiseCov;
    cv::setIdentity(filter->processNoiseCov,cv::Scalar::all(self.processNoiseCov));
}

- (void) setMeasurementNoiseCov:(double)measurementNoiseCov {
    _measurementNoiseCov = measurementNoiseCov;
    cv::setIdentity(filter->measurementNoiseCov,cv::Scalar::all(self.measurementNoiseCov));
}

@end
