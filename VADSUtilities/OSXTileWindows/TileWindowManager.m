//
//  TileWindowManager.m
//  TestNSWindows
//
//  Created by JiangZhiping on 16/3/31.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//
#import <CocoaXtensions/CocoaXtensions.h>
#import "TileWindowManager.h"

@implementation TileWindowManager {
    dispatch_queue_t windowManagerQueue;
}

+ (instancetype) getInstance {
    static TileWindowManager * instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[TileWindowManager alloc] init];
    });
    return instance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        _windowDictionary = [[NSMutableDictionary alloc] init];
        windowManagerQueue = dispatch_queue_create("windowManagerQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    
    return self;
}

- (void) addWindowTitledWith:(NSString *)uniqueWindowTitle {
    dispatch_async(dispatch_get_main_queue(), ^ {
        TileWindow * window = [[TileWindow alloc] initWithTileString:uniqueWindowTitle];
        window.backgroundColor = [NSUIColor randomColor];
        if (window) {
            _windowDictionary[uniqueWindowTitle] = window;
        }
    });
}

- (void) addWindowTitledWith:(NSString *)uniqueWindowTitle AndColor:(NSColor *)color {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        TileWindow * window = [[TileWindow alloc] initWithTileString:uniqueWindowTitle];
        window.backgroundColor = color;
        if (window) {
            _windowDictionary[uniqueWindowTitle] = window;
        }
    });
    

}

- (void) moveWindowNamed:(NSString *)windowTitle ToPosition:(CGPoint)point {
    dispatch_async(dispatch_get_main_queue(), ^{
        TileWindow * window = [self getWindowTitledWith:windowTitle];
        
        if (window == nil) {
            [self addWindowTitledWith:windowTitle];
            window = [self getWindowTitledWith:windowTitle];
        }
        
        if (window) {
            [window setCentralPosition:point];
        }
    });
}

- (TileWindow *) getWindowTitledWith:(NSString *)windowTitle {
    return [_windowDictionary valueForKey:windowTitle];
}

- (void) removeWindowTitledWith:(NSString *)windowTitle {
    [_windowDictionary removeObjectForKey:windowTitle];
}

- (void) removeAllWindow {
    [_windowDictionary removeAllObjects];
}


@end
