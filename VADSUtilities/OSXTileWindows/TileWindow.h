//
//  TileWindow.h
//  TestNSWindows
//
//  Created by JiangZhiping on 16/3/31.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TileWindow : NSWindow

@property NSPoint initialPosition;

@property (nonatomic) NSTextField * label;

- (instancetype) initWithTileString:(NSString *)titleString;

- (void) setCentralPosition:(NSPoint) centralPoint;

@end
